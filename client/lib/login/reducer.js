import { combineReducers } from "redux";
import userReducer from "./reducers/userReducer";
import formReducer from "./reducers/formReducer";

export default combineReducers({
  userReducer,
  formReducer,
});
