export const DISTANCE_BETWEEN_VERTICES = 200;

class Point {
  constructor(opts) {
    // eslint-disable-next-line object-curly-newline
    const { x, y, angle, velocity } = opts;
    this.x = x;
    this.y = y;
    this.angle = angle;
    this.velocity = velocity;
  }

  move() {
    this.x = this.x + Math.cos(this.angle) * this.velocity;
    this.y = this.y + Math.sin(this.angle) * this.velocity;
  }

  getNextCoordinates() {
    this.nextX = this.x + Math.cos(this.angle) * this.velocity;
    this.nextY = this.y + Math.sin(this.angle) * this.velocity;
    this.nextCoordinates = { x: this.nextX, y: this.nextY };
    return this.nextCoordinates;
  }
}

class Graph {
  constructor(opts = {}) {
    const { points, connectionFn, canvas } = opts;
    const defaultConnectionFn = () => true;
    this.points = points || [];
    this.connectionFn = connectionFn || defaultConnectionFn;
    this.canvas = canvas;

    this.initConnections();
    this.calculateConnections();
  }

  initConnections() {
    const { points } = this;
    if (points && points.length) {
      this.connections = new Array(points.length);
      points.forEach((point, i) => {
        this.connections[i] = new Array(points.length);
      });
    } else {
      this.connections = [];
    }
  }

  addPoint(point) {
    this.points.push(point);
    this.initConnections();
    this.calculateConnections();
  }

  getPoints() {
    return this.points;
  }

  getConnections() {
    return this.connections;
  }

  move() {
    this.points.forEach(point => point.move());
    this.calculateConnections();
    this.points.forEach(point => {
      const nextCoords = point.getNextCoordinates();
      if (nextCoords.x >= this.canvas.width || nextCoords.x <= 0) {
        // eslint-disable-next-line no-param-reassign
        point.angle += Math.PI;
      }
      if (nextCoords.y >= this.canvas.height || nextCoords.y <= 0) {
        // eslint-disable-next-line no-param-reassign
        point.angle += Math.PI;
      }
      point.move();
    });
  }

  calculateConnections() {
    const { points } = this;
    if (points.length > 1) {
      points.forEach((a, outerIndex) => {
        const innerList = points.slice(0, outerIndex + 1);
        innerList.forEach((b, innerIndex) => {
          let hasConnection;
          if (outerIndex !== innerIndex) {
            hasConnection = this.connectionFn(a, b);
          } else {
            hasConnection = false;
          }
          this.connections[outerIndex][innerIndex] = hasConnection;
        });
      });
    }
  }
}

class Demo {
  constructor(opts) {
    const { id, connectionFn } = opts;
    this.canvas = document.getElementById(id);
    this.graph = new Graph({ connectionFn, canvas: this.canvas });
    // eslint-disable-next-line no-restricted-globals
    this.canvas.width = screen.availWidth;
    // eslint-disable-next-line no-restricted-globals
    this.canvas.height = screen.availHeight;
    this.ctx = this.canvas.getContext("2d");
    // eslint-disable-next-line no-use-before-define
    this.drawer = new Drawer(this.ctx);
    this.setupEmergenceArea();
  }

  random(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
  }

  setupEmergenceArea() {
    for (let i = 0; i < 5; i += 1) {
      this.region = [];
      this.region.x = this.random(100, this.canvas.width - 100);
      this.region.y = this.random(100, this.canvas.height - 100);
      const quantity = this.random(5, 10);

      // eslint-disable-next-line no-shadow
      for (let i = 0; i < quantity; i += 1) {
        const x = this.random(this.region.x - 100, this.region.x + 100);
        const y = this.random(this.region.y - 100, this.region.y + 100);
        const velocity = 0.2;
        const angle = Math.random() * (Math.PI * 1);
        const options = {
          x,
          y,
          velocity,
          angle,
        };
        const point = new Point(options);
        this.graph.addPoint(point);
      }
    }
  }

  clear() {
    const { ctx } = this;
    ctx.save();
    ctx.fillStyle = "#e8eaec";
    ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);
    ctx.restore();
  }

  mainLoop() {
    this.render();
    this.graph.move();
  }

  render() {
    const points = this.graph.getPoints();
    const connections = this.graph.getConnections();
    this.clear();
    if (points.length) {
      this.drawer.drawConnections(points, connections);
      points.forEach(point => {
        this.drawer.drawPoint(point);
      });
    }
    this.drawer.drawVignette(this.canvas.width, this.canvas.height);
    requestAnimationFrame(this.mainLoop.bind(this));
  }
}

class Drawer {
  constructor(ctx) {
    this.ctx = ctx;
  }

  drawPoint(point) {
    const { ctx } = this;
    ctx.save();
    ctx.beginPath();
    ctx.translate(point.x, point.y);
    ctx.arc(0, 0, 3, 0, Math.PI * 2, false);
    ctx.fillStyle = "white";
    ctx.strokeStyle = "black";
    ctx.lineWidth = 1;
    ctx.fill();
    ctx.stroke();
    ctx.restore();
  }

  drawConnections(points, connections) {
    points.forEach((a, outerIndex) => {
      const innerList = points.slice(0, outerIndex + 1);
      innerList.forEach((b, innerIndex) => {
        if (connections[outerIndex][innerIndex]) {
          this.drawConnection(a, b);
        }
      });
    });
  }

  drawConnection(start, end) {
    const { ctx } = this;
    ctx.beginPath();
    ctx.strokeStyle = "black";
    ctx.moveTo(start.x, start.y);
    ctx.lineTo(end.x, end.y);
    ctx.stroke();
  }

  drawVignette(width, height) {
    const { ctx } = this;
    ctx.save();

    const grd = ctx.createLinearGradient(0, 0, 0, height);
    grd.addColorStop(0, "#0346b2");
    grd.addColorStop(1, "transparent");
    ctx.fillStyle = grd;
    ctx.fillRect(0, 0, width, height);
    ctx.restore();
  }
}

export default Demo;
