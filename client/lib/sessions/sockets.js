import io from "socket.io-client";
import store from "../index";
import { recievedMessagesList, messageReceived } from "../chat";
import {
  setParticipants,
  addParticipant,
  removeParticipant,
  initPublisher,
  updateParticipant,
} from "../call";
import { showFlag } from "../shared/flag";

class Socket {
  connect(nameSpase) {
    this.nsp = io(`/${nameSpase}`);
    this.nameSpase = nameSpase;

    console.log(`Connected to room ${this.nameSpase}`);

    this.nsp.on("userJoin", user => {
      this.nsp.on("memberList", users => {
        const members = JSON.parse(users);
        console.log(members);

        store.dispatch(setParticipants(members));
        store.dispatch(initPublisher());
      });

      this.nsp.on("messagesList", msgs => {
        const messages = JSON.parse(msgs);
        store.dispatch(recievedMessagesList(messages));
      });

      const member = JSON.parse(user);
      store.dispatch(addParticipant(member));

      console.log(`User ${member.login} joined`);
    });

    this.nsp.on("messageSent", body => {
      const result = JSON.parse(body);
      const { message, user } = result;
      store.dispatch(messageReceived(result));
      console.log(`User ${user} sent message ${message}`);
    });

    this.nsp.on("muteAudio", (user, isMuted) => {
      const member = JSON.parse(user);
      if (isMuted) {
        console.log(`User ${member.login} muted his mic`);
      } else {
        console.log(`User ${member.login} unmuted his mic`);
      }
      store.dispatch(
        updateParticipant(member.login, { isAudioEnabled: !isMuted }),
      );
    });

    this.nsp.on("muteVideo", (user, isMuted) => {
      const member = JSON.parse(user);
      if (isMuted) {
        console.log(`User ${member.login} muted his camera`);
      } else {
        console.log(`User ${member.login} unmuted his camera`);
      }
      store.dispatch(
        updateParticipant(member.login, { isVideoEnabled: !isMuted }),
      );
    });

    this.nsp.on("raiseHand", (user, isRaised) => {
      const member = JSON.parse(user);
      const {
        login: {
          userReducer: { login: currentUserLogin },
        },
      } = store.getState();

      if (isRaised && currentUserLogin !== member.login) {
        console.log(`User ${member.login} wants to speak`);
        store.dispatch(
          showFlag("success", `User ${member.login} wants to speak`),
        );
      }
    });

    this.nsp.on("userLeft", args => {
      const user = JSON.parse(args);
      console.log(`User ${user.login} left`);

      store.dispatch(removeParticipant(user));
      store.dispatch(showFlag("success", `User ${user.login} left`));
    });
  }

  emit(event, data) {
    this.nsp.emit(event, data);
  }

  disconnect() {
    this.nsp.disconnect(true);
    console.log(`Disconnected from room ${this.nameSpase}`);
  }
}

export default new Socket();
