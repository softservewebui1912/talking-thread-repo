import Glide from "@glidejs/glide";

export default function() {
  // slider - Glide
  new Glide("#glide1").mount();
  new Glide("#glide2").mount();

  // scroll and button nav

  window.document.querySelectorAll(".nav a, .down").forEach(navLink => {
    navLink.addEventListener("click", e => {
      const hashval = navLink.getAttribute("href");
      const target = window.document.querySelector(hashval);

      target.scrollIntoView({
        behavior: "smooth",
        block: "start",
      });
      window.history.pushState(null, null, hashval);
      e.preventDefault();
    });
  });

  return null;
}
