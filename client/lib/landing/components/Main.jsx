import React from "react";
import About from "./About";
import Slider from "./Slider";
import Reviews from "./Reviews";
import Join from "../containers/Join";
import initLanding from "../initLanding";

class Main extends React.Component {
  componentDidMount() {
    initLanding();
  }

  render() {
    return (
      <div className="main">
        <About />
        <Slider />
        <Reviews />
        <Join />
      </div>
    );
  }
}

export default Main;
