import React from "react";

const About = () => (
  <div className="section section--about" id="about">
    <div className="container container--about">
      <h2 className="heading-section heading-section--about">
        The essence of the polished experience
      </h2>
      <h3 className="subheading-section">About us</h3>
      <div className="about-content">
        <img
          className="about-content__img"
          src="./images/about.png"
          alt="About us"
        />
        <p className="text about-content__text">
          TalkingThread – full-featured WebRTC-capable application based on
          OpenVidu platform, which can be used for creating video-call
          sessionsThis application is designed for those who want be in touch
          with their friends, relatives and colleagues but don’t like to install
          additional software. TalkingThread provides video, voice and text
          communications. And all one needs to use it is Internet regardless of
          browser! And of course, TalkingThread is completely free to use.
        </p>
      </div>
      <button className="button--landing button--about" type="button">
        Create your own room
      </button>
    </div>
  </div>
);

export default About;
