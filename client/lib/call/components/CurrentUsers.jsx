import React from "react";
import Avatar, { AvatarItem } from "@atlaskit/avatar";
import PropTypes from "prop-types";

function CurrentUsers(props) {
  return (
    <div className="usersBlock">
      <div className="users">
        {props.users.map(user => (
          <AvatarItem
            key={user.login}
            avatar={
              <Avatar
                name={user.name}
                src={`https://robohash.org/${user.name}?set=set4`}
                size="large"
                presence="online"
              />
            }
            primaryText={user.name}
          />
        ))}
      </div>
    </div>
  );
}

CurrentUsers.propTypes = {
  users: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default CurrentUsers;
