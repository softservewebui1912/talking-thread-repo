/* eslint-disable jsx-a11y/no-static-element-interactions, jsx-a11y/click-events-have-key-events */

import React, { Fragment, Component } from "react";
import PropTypes from "prop-types";
import VidAudioMutedIcon from "@atlaskit/icon/glyph/vid-audio-muted";
import VidCameraOffIcon from "@atlaskit/icon/glyph/vid-camera-off";

class SmallVideo extends Component {
  renderVideoContainer() {
    const { participant, isScreenSharingEnabled } = this.props;
    const classNames = [
      "small-video-container__video",
      participant.isRemote || isScreenSharingEnabled ? "remote" : null,
      !participant.isVideoEnabled
        ? "small-video-container__video_hidden"
        : null,
    ].filter(Boolean);

    return (
      <div
        id={`smallVideo-${participant.login}`}
        className={classNames.join(" ")}
      />
    );
  }

  renderAvatar() {
    const { participant } = this.props;
    const imgSrc = `https://robohash.org/${participant.login}?set=set4`;

    return (
      <img
        className="small-video-container__roboto"
        alt="User Avatar"
        src={imgSrc}
      />
    );
  }

  renderUserData() {
    const { participant } = this.props;
    return (
      <Fragment>
        <div className="small-video-container__name">{participant.login}</div>
        <div className="small-video-container__video-toolbar">
          {!participant.isVideoEnabled && (
            <VidCameraOffIcon
              className="small-video-container__icon"
              primaryColor="rgba(213, 4, 29, 0.8)"
              size="medium"
            />
          )}
          {!participant.isAudioEnabled && (
            <VidAudioMutedIcon
              className="small-video-container__icon"
              primaryColor="rgba(213, 4, 29, 0.8)"
              size="medium"
            />
          )}
        </div>
      </Fragment>
    );
  }

  render() {
    const { participant, onClick } = this.props;
    const classNames = [
      "small-video-container__participant",
      participant.isRemote ? "remote" : null,
    ].filter(Boolean);

    return (
      <div className={classNames.join(" ")} onClick={onClick}>
        {this.renderUserData()}
        {this.renderAvatar()}
        {this.renderVideoContainer()}
      </div>
    );
  }
}

SmallVideo.propTypes = {
  isScreenSharingEnabled: PropTypes.bool.isRequired,
  participant: PropTypes.shape({
    name: PropTypes.string,
    login: PropTypes.string,
    isRemote: PropTypes.bool,
    mediaStream: PropTypes.any,
    connectionId: PropTypes.string,
    isVideoEnabled: PropTypes.bool,
    isAudioEnabled: PropTypes.bool,
  }).isRequired,
  onClick: PropTypes.func.isRequired,
};
export default SmallVideo;
