import React, { Component } from "react";

class TemporaryVideo extends Component {
  constructor(props) {
    super(props);
    this.videoElement = React.createRef();
  }

  async componentDidMount() {
    const constraints = { audio: true, video: true };
    try {
      const stream = await navigator.mediaDevices.getUserMedia(constraints);
      const video = this.videoElement.current;
      video.srcObject = stream;
      video.onloadedmetadata = () => {
        video.play();
      };
    } catch (e) {
      // eslint-disable-next-line no-console
      console.log(`${e.name}: ${e.message}`);
    }
  }

  render() {
    return (
      <div
        style={{
          width: "100vw",
          height: "100vh",
          overflow: "hidden",
          filter: "blur(3px)",
        }}
      >
        <video
          ref={this.videoElement}
          style={{
            minWidth: "100%",
            minHeight: "100%",
            transform: "scale(-1, 1)",
          }}
          autoPlay
          playsInline
        >
          <track kind="captions" />
        </video>
      </div>
    );
  }
}

export default TemporaryVideo;
