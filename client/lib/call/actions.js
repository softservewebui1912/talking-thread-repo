import { push } from "connected-react-router";
import localStorage from "../sessions/LocalStorageService";
import socket from "../sessions/sockets";
import { setSessionName } from "../main/actions";
import { OpenViduSessionService } from "../sessions";
import { showFlag } from "../shared/flag";
import {
  SET_MAIN_PARTICIPANT,
  ADD_PARTICIPANT,
  REMOVE_PARTICIPANT,
  OPEN_SECTION,
  SET_PARTICIPANTS,
  UPDATE_PARTICIPANT,
  SCREEN_SHARING_ENABLED,
} from "./actionTypes";
import { resetMessages } from "../chat";

export const openSection = section => ({
  type: OPEN_SECTION,
  section,
});

export const updateParticipant = (login, participant) => ({
  type: UPDATE_PARTICIPANT,
  login,
  participant,
});

export const leaveSession = () => async (dispatch, getState) => {
  if (OpenViduSessionService.session) {
    OpenViduSessionService.unpublish();
    OpenViduSessionService.leaveSession();
    socket.disconnect();
  }

  const {
    main: { sessionName },
  } = getState();
  dispatch(openSection(null));
  dispatch(resetMessages());
  dispatch(setSessionName(""));
  dispatch(push("/main"));
  dispatch(showFlag("success", `You successfully left ${sessionName} room`));
};

export const joinSession = () => async (dispatch, getState) => {
  const user = getState().login.userReducer;
  const { sessionName } = getState().main;

  await OpenViduSessionService.connect(user, sessionName);
  socket.connect(sessionName);
  socket.emit("userJoin", {
    id: localStorage.get("id"),
    login: user.login,
    role: user.role,
  });

  dispatch(showFlag("success", `You've successfully joined ${sessionName}`));
};

export const initPublisher = () => async (dispatch, getState) => {
  const user = getState().login.userReducer;
  const { sessionName } = getState().main;

  await OpenViduSessionService.joinSession(user, sessionName);
  OpenViduSessionService.initPublisher(user);
};

export const shareScreen = () => (dispatch, getState) => {
  const user = getState().login.userReducer;
  OpenViduSessionService.unpublish();
  OpenViduSessionService.initPublisher(user, "screen", error => {
    if (error.name === "SCREEN_EXTENSION_NOT_INSTALLED") {
      dispatch(
        showFlag(
          "error",
          "You should install an extension to be able to share your screen",
          "It can be found here for Chrome:",
          [
            {
              content: "OpenVidu ScreenSharing - Chrome Web Store",
              href:
                "https://chrome.google.com/webstore/detail/openvidu-screensharing/lfcgfepafnobdloecchnfaclibenjold",
              target: "_blank",
            },
          ],
        ),
      );
    } else if (error.name === "SCREEN_SHARING_NOT_SUPPORTED") {
      dispatch(
        showFlag("error", "Your browser does not support screen sharing"),
      );
    } else if (error.name === "SCREEN_EXTENSION_DISABLED") {
      dispatch(
        showFlag("error", "You need to enable screen sharing extension"),
      );
    } else if (error.name === "SCREEN_CAPTURE_DENIED") {
      dispatch(
        showFlag(
          "error",
          "You need to choose a window or application to share",
        ),
      );
    } else {
      dispatch({
        type: SCREEN_SHARING_ENABLED,
        payload: true,
      });
    }
  });
};

export const unshareScreen = () => (dispatch, getState) => {
  const user = getState().login.userReducer;
  OpenViduSessionService.unpublish();

  dispatch({
    type: SCREEN_SHARING_ENABLED,
    payload: false,
  });

  OpenViduSessionService.initPublisher(user);
};

export const openFullScreen = rootElement => () => {
  if (rootElement.requestFullscreen) {
    rootElement.requestFullscreen();
  } else if (rootElement.mozRequestFullScreen) {
    /* Firefox */
    rootElement.mozRequestFullScreen();
  } else if (rootElement.webkitRequestFullscreen) {
    /* Chrome, Safari and Opera */
    rootElement.webkitRequestFullscreen();
  } else if (rootElement.msRequestFullscreen) {
    /* IE/Edge */
    rootElement.msRequestFullscreen();
  }
};

export const closeFullScreen = () => () => {
  if (document.exitFullscreen) {
    document.exitFullscreen();
  } else if (document.mozCancelFullScreen) {
    /* Firefox */
    document.mozCancelFullScreen();
  } else if (document.webkitExitFullscreen) {
    /* Chrome, Safari and Opera */
    document.webkitExitFullscreen();
  } else if (document.msExitFullscreen) {
    /* IE/Edge */
    document.msExitFullscreen();
  }
};

export const raiseHand = isToggled => () => {
  socket.emit("raiseHand", isToggled);
};

export const setMainParticipant = login => ({
  type: SET_MAIN_PARTICIPANT,
  login,
});

export const addParticipant = user => (dispatch, getState) => {
  const currentUser = getState().login.userReducer;

  dispatch({
    type: ADD_PARTICIPANT,
    user,
    currentUser,
  });
};

export const setParticipants = users => (dispatch, getState) => {
  const currentUser = getState().login.userReducer;

  dispatch({
    type: SET_PARTICIPANTS,
    users,
    currentUser,
  });
};

export const removeParticipant = user => (dispatch, getState) => {
  dispatch({
    type: REMOVE_PARTICIPANT,
    user,
  });
  const { mainParticipant, participants } = getState().call.videoReducer;

  if (
    !participants.find(participant => participant.login === mainParticipant)
  ) {
    const [nextMainParticipant] = participants;

    dispatch(setMainParticipant(nextMainParticipant.login));
  }
};

export const publishVideo = () => (dispatch, getState) => {
  const { participants } = getState().call.videoReducer;
  const publisher = participants.find(
    participant => participant.isRemote === false,
  );
  const isVideoEnabled = !publisher.isVideoEnabled;
  OpenViduSessionService.publisher.publishVideo(isVideoEnabled);

  // dispatch(updateParticipant(publisher.login, { isVideoEnabled }));
  socket.emit("muteVideo", !isVideoEnabled);
};

export const publishAudio = () => (dispatch, getState) => {
  const { participants } = getState().call.videoReducer;
  const publisher = participants.find(
    participant => participant.isRemote === false,
  );
  const isAudioEnabled = !publisher.isAudioEnabled;
  OpenViduSessionService.publisher.publishAudio(isAudioEnabled);

  // dispatch(updateParticipant(publisher.login, { isAudioEnabled }));
  socket.emit("muteAudio", !isAudioEnabled);
};
