import IS_LOADING from "./actionTypes";

const initialState = {
  isLoading: true,
};

export default function(state = initialState, action) {
  switch (action.type) {
    case IS_LOADING:
      return {
        ...state,
        isLoading: action.isLoading,
      };
    default:
      return state;
  }
}
