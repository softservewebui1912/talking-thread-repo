import React from "react";
import PropTypes from "prop-types";

const SystemMessage = props => <div className="systemMsg">{props.msgBody}</div>;

SystemMessage.propTypes = {
  msgBody: PropTypes.string,
};

export default SystemMessage;
