const requireDirectory = require("require-directory");
function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}
module.exports = requireDirectory(module, {
  rename: capitalizeFirstLetter,
});
