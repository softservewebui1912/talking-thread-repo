const database = require("../../database");

class UpdateRoom {
  async execute(id, body) {
    let result = false;
    try {
      const room = await database.Room.findById(id);
      if (room !== null) {
        await room.update({ name: body.name });
        console.log("Room #" + id + " updated");
        result = room;
      } else {
        console.log("Room does not exist");
      }
    } catch (err) {
      console.log(err);
    }
    return result;
  }
}

module.exports = UpdateRoom;
