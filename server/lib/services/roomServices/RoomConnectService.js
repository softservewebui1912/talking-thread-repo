const database = require("../../database");
const MapSessions = require("../MapSessionsService");

class RoomConnect {
  async execute(name, body) {
    let result = false;
    const user = await database.User.findOne({
      where: {
        login: body.login,
      },
      attributes: {
        exclude: ["password", "refreshToken", "token"],
      },
    });
    const room = await database.Room.findOne({ where: { name } });
    if (user !== null && room !== null) {
      const role = user.role;
      const serverData = JSON.stringify({ serverData: user });
      const tokenOptions = {
        data: serverData,
        role,
      };
      const session = MapSessions.get(room.id);
      if (session) {
        try {
          const token = await session.generateToken(tokenOptions);
          user.update({ roomId: room.id, token });
          console.log("User " + user.login + " conected to room #" + room.id);

          result = { token };
        } catch (err) {
          console.log(err);
        }
      } else {
        console.log("Room does not exist");
      }
    } else {
      console.log("User/room does not exist");
    }
    console.log("result :", result);
    return result;
  }
}

module.exports = RoomConnect;
