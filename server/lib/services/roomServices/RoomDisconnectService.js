const database = require("../../database");
const MapSessions = require("../MapSessionsService");

class RoomDisconnect {
  async execute(name, body) {
    let result = false;
    const room = await database.Room.findOne({ where: { name } });
    const user = await database.User.findOne({
      where: { login: body.login },
      attributes: {
        exclude: ["password"],
      },
    });
    if (room !== null && user !== null) {
      if (user.token !== null) {
        console.log(
          "Removing user | {sessionName, token}={" +
            room.name +
            ", " +
            user.token +
            "}"
        );
        try {
          await user.update({ token: null, roomId: null });
          console.log(
            "User " + user.login + " has disconnected from " + room.name
          );
          const users = await database.User.findAll({
            where: { roomId: room.id },
          });
          let empty = false;
          if (users.length === 0) {
            await room.destroy();
            MapSessions.del(room.id);
            console.log("Room #" + room.id + " deleted");
            empty = true;
          }
          result = { result: true, empty };
        } catch (err) {
          console.log(err);
        }
      } else {
        console.log("User not connected to any room");
      }
    }
    return result;
  }
}

module.exports = RoomDisconnect;
