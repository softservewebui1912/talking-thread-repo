const database = require("../../database");

class DeleteUser {
  async execute(id) {
    let result = false;
    try {
      const user = await database.User.findById(id);
      if (user) {
        user.destroy();
        console.log("User deleted");
        result = true;
      } else {
        console.log("User does not exist");
      }
    } catch (err) {
      console.log(err);
    }
    return result;
  }
}
module.exports = DeleteUser;
