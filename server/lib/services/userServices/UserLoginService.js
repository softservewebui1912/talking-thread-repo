const database = require("../../database");
const jwt = require("jsonwebtoken");
const uuid = require("uuid");
const cfg = require("../../routes/config");
const bcrypt = require("bcryptjs");

class UserLogin {
  async execute(login, password) {
    let result = false;
    const user = await database.User.findOne({
      where: { login },
    });
    if (user !== null) {
      const flag = await bcrypt.compare(password, user.password);
      if (flag) {
        try {
          const accessToken = jwt.sign(
            { id: user.id, iss: cfg.issuer, aud: cfg.audience },
            cfg.secret
          );
          const refreshToken = uuid();
          user.update({ refreshToken });
          result = { accessToken, refreshToken, id: user.id };
        } catch (err) {
          console.log(err);
        }
      }
    } else {
      console.log("User does not exist");
    }
    return result;
  }
}

module.exports = UserLogin;
