const database = require("../../database");

class GetMessages {
  async execute(roomName) {
    let result = false;
    try {
      const room = await database.Room.findOne({ where: { name: roomName } });
      if (room) {
        const messages = await database.Message.findAll({
          where: { roomId: room.id },
          include: [{ required: true, model: database.User }],
          attributes: {
            exclude: ["password", "refreshToken", "token"],
          },
        });

        result = messages.map(message => ({
          message: message.value,
          user: message.user.login,
        }));
      }
    } catch (err) {
      console.log(err);
    }
    return result;
  }
}

module.exports = GetMessages;
