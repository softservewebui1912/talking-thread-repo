const Joi = require("joi");

const schema = Joi.object().keys({
  id: Joi.number().min(1),
  name: Joi.string(),
});

module.exports = schema;
