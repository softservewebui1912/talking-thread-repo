/*eslint no-sync: ["error", { allowAtRootLevel: true }]*/
/* CONFIGURATION */

const database = require("./database");
const Routers = require("./routes");
const path = require("path");
const passport = require("./services/Auth/checkAuth");
const PORT = process.env.PORT || 5000;
const io = require("./chat/EmitService");

process.env.NODE_ENV = process.env.NODE_ENV || "development";

database.connect();
// Check launch arguments: must receive openvidu-server URL and the secret
if (process.argv.length !== 4) {
  console.log(
    "Usage: node " +
      path.resolve(__dirname, "server.js") +
      " OPENVIDU_URL OPENVIDU_SECRET"
  );
  throw new Error("Need 4 values in command line");
}
// For demo purposes we ignore self-signed certificate
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

// Node imports

const express = require("express");
const fs = require("fs");
const session = require("express-session");
const http = require("http");
const https = require("https");
const bodyParser = require("body-parser"); // Pull information from HTML POST (express4)
const app = express(); // Create our app with express

// Server configuration
app.use(
  session({
    saveUninitialized: true,
    resave: false,
    secret: "MY_SECRET",
  })
);
app.use(passport.initialize());
app.use(passport.session());

app.use(express.static(path.resolve(__dirname, "../public"))); // Set the static files location

app.use(
  bodyParser.urlencoded({
    extended: "true",
  })
); // Parse application/x-www-form-urlencoded
app.use(bodyParser.json()); // Parse application/json
app.use(
  bodyParser.json({
    type: "application/vnd.api+json",
  })
); // Parse application/vnd.api+json as json

if (process.env.NODE_ENV === "development") {
  // Listen (start app with node server.js)
  const options = {
    key: fs.readFileSync("openvidukey.pem"),
    cert: fs.readFileSync("openviducert.pem"),
  };

  const server = https.createServer(options, app).listen(PORT);
  io.mount(server);
} else {
  const server = http.createServer(app).listen(PORT);
  io.mount(server);
}

console.log(`App listening on port ${PORT}`);

/* CONFIGURATION */

app.use("/api/users", Routers.UserRouter);
app.use("/api/rooms", Routers.RoomRouter);
app.use("/api/rooms/chat", Routers.ChatRouter);
app.use("/api/auth", Routers.AuthRouter);

app.get("*", (req, res) => {
  res.sendFile(path.resolve(__dirname, "../public/index.html"));
});
